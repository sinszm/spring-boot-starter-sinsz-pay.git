# spring-boot-starter-sinsz-pay 支付SDK

#### 项目介绍

```
程序猿们在常规项目中往往会涉及到一些支付相关的功能，但是往往被支付集成的一些繁琐问题所困惑。本项目正是为了解决这个困惑而诞生了。

本项目主要框架是基于springboot扩展的，当前版本的发布程序猿们可谨慎选择，项目管理员（本人)已经实测现阶段的功能可正常使用。
```
` 最新版本：0.0.4 `，更新内容：处理异常返回机制；新增支付相关结果返回接口；

#### 安装教程
maven方式:
```
<dependency>
    <groupId>com.sinsz</groupId>
    <artifactId>spring-boot-starter-sinsz-pay</artifactId>
    <version>0.0.4</version>
</dependency>
```
gradle方式:
```
compile group: 'com.sinsz', name: 'spring-boot-starter-sinsz-pay', version: '0.0.4'
```

#### 使用说明

一). 配置文件

现阶段支持微信App支付、微信公众号支付、支付宝App支付，微信支付配置中需要指定支付平台类型`platform` ，
其中配置中涉及到的接口或调整页面配置均为网址绝对路径，涉及到证书和公钥私钥配置路径均为物理绝对路径，
配置文件中部分参数为必填参数，项目启动会对参数进行校验，针对微信付款功能，现目前仅支持付款到零钱。
微信公众号支付时，提供有获取授权的接口
请在spring的配置文件加入以下配置（配置支持单支付)：

```
sinsz:
  pay:
    http:                   //接口https、http根地址
    wxpay:                  //微信配置（配置一旦启用后续参数会启用验证)
      platform: public      //平台选择
      appid: 
      secret: 
      mch-id: 
      mch-key: 
      mch-cer:              //支付证书物理地址
      pay-notify: 
      refund-notify: 
      we-chat-public-number-redirect:   //微信公众号支付时配置用户自己的已经获取过授权的页面
    alipay:                 //支付宝配置（配置一旦启用后续参数会启用验证)
      appid: 
      private-key:          //私钥物理地址
      public-key:           //公钥物理地址
      pay-notify: 
      refund-notify: 
```

示例配置：
```
sinsz:
  pay:
    http: https://wx.chdntxdsesg.com
    wxpay:
      platform: public
      appid: w12bdxxx3232f5af90
      secret: aa140d234237587034334e9b87a41
      mch-id: 1464222502
      mch-key: 30B9C3432342CC0B776C80D3061FD1AFC
      mch-cer: /Users/chenjianbo/Desktop/cert/public2/apiclient_cert.p12
      pay-notify: /api/wx_pay_notify
      refund-notify: /api/wx_refund_notify
      we-chat-public-number-redirect: /pay.html
#    wxpay:
#      platform: open
#      appid: w133af123433dba4236
#      secret: 145b282f977e23212d32f1c00778
#      mch-id: 14674353675
#      mch-key: 30B9C650BF2CC234234C80D3061FD1AFC
#      mch-cer: /Users/chenjianbo/Desktop/cert/open/apiclient_cert.p12
#      pay-notify: /api/wx_pay_notify
#      refund-notify: /api/wx_refund_notify
    alipay:
      appid: 2027035555555319
      private-key: /Users/chenjianbo/Desktop/cert/alipay/AlipayPrivateKey
      public-key: /Users/chenjianbo/Desktop/cert/alipay/AlipayPublicKey
      pay-notify: /api/ali_pay_notify
      refund-notify: /api/ali_refund_notify
      
```
微信公众号的默认请求地址：`https://wx.chdntxdsesg.com/api/1.0/oa/fetch/user`（`api`层是动态匹配的，根据用户自行配置而定)

二). 使用

第一步、项目添加当前依赖包，并填写相关支付配置即可生效。

第二步、在代码中注入实例并执行代码，如下：

```

@Autowired
private Route route;

Pay pay = route.pay(PayMethod.WECHAT_APP);

```

Pay.class 接口说明如下：
```
    /**
     * 统一下单，预支付创建
     * @param outTradeNo    自定义商户订单号
     * @param body          主描述信息
     * @param detail        描述信息
     * @param fee           费用，精度：分
     * @param openid        公众号时为公众号唯一的用户ID
     * @return              支付串
     */
    String unifiedOrder(String outTradeNo, String body, String detail, int fee, String openid);

    //示例返回值：
    // 不同平台返回值不一样，需前端自行处理
```
```
    /**
     * 查询订单
     * @param transactionId     支付平台的订单号
     * @param outTradeNo        自定义商户订单号
     * @return                  支付结果
     */
    String orderQuery(String transactionId, String outTradeNo);
    
    //示例返回值：
    //{
    //    "tradeState":"QUERY_PAY_SUCCESS",
    //    "totalFee":"1.00",
    //    "transactionId":"2018110922001408991009210367",
    //    "outTradeNo":"GF1541744798570V748070",
    //    "timeEnd":"20181109142642",
    //    "tradeStateDesc":"支付成功"
    //}
```
```
    /**
     * 关闭订单
     * @param outTradeNo        自定义的商户订单号
     * @return                  成功状态或错误信息
     */
    String closeOrder(String outTradeNo);

    //示例返回值：
    // SUCCESS / FAIL
```
```
    /**
     * 申请退款
     * <p>
     *     未入账(未结算)订单可退款，已入账订单建议使用人工客服方式退款。
     * </p>
     * @param transactionId     支付平台的订单号
     * @param outTradeNo        自定义商户订单号
     * @param outRefundNo       退款单号
     * @param totalFee          总金额/剩余退款
     * @param fee               退款金额
     * @param refundDesc        退款原因
     * @return                  退款信息
     */
    String refund(String transactionId, String outTradeNo, String outRefundNo, int totalFee, int fee, String refundDesc);

    //示例返回值：
    //{
    //    "transactionId":"4200000227201811097396370236",
    //    "outTradeNo":"84952525326239570354942093404490",
    //    "outRefundNo":"80379324016849989383083842550171",
    //    "refundId":"50000408952018110906966070323",
    //    "applyTime":1541745847217
    //}
```
```
    /**
     * 退款查询
     * @param refundId          支付平台退款交易号
     *                          <p>
     *                              如果是支付宝平台：refundId则为outTradeNo
     *                          </p>
     * @param outRefundNo       自定义商户退款单号
     * @return                  退款结果
     */
    String refundQuery(String refundId, String outRefundNo);

    //示例返回值：
    //{
    //    "transactionId":"4200000227201811097396370236",
    //    "outTradeNo":"84952525326239570354942093404490",
    //    "outRefundNo":"80379324016849989383083842550171",
    //    "refundId":"50000408952018110906966070323",
    //    "refundCount":1,
    //    "refundStatus":"QUERY_REFUND_SUCCESS",
    //    "refundSuccessTime":"2018-11-09 14:44:07"
    //}
```
```
    /**
     * 下载对账单
     * @param date          对账日期
     * @param billType      对账类型
     * @return              文件流
     */
    ResponseEntity<InputStreamResource> downloadBill(Date date, BillType billType);
```
```
    /**
     * 支付结果通知
     * <p>
     *     由支付平台自动回执解析，
     *     用于回调接口中获取并解析回执参数对象
     * </p>
     * @return          结果
     */
    String payNotify();

    //示例返回值：
    //{
    //    "tradeState":"QUERY_PAY_SUCCESS",
    //    "totalFee":"1.00",
    //    "transactionId":"2018110922001408991009210367",
    //    "outTradeNo":"Gxx1541744798570V748070",
    //    "timeEnd":"20181109142642",
    //    "tradeStateDesc":"支付成功"
    //}
```
```
    /**
     * 退款结果通知
     * <p>
     *     由支付平台自动回执解析，
     *     用于回调接口中获取并解析回执参数对象
     *     微信支付用户暂时不建议使用该接口，如需要使用，需用户自行实现加密串解密
     * </p>
     * @return          结果
     */
    String refundNotify();

    //示例返回值：
    //{
    //    "transactionId":"4200000227201811097396370236",
    //    "outTradeNo":"84952525326239570354942093404490",
    //    "outRefundNo":"80379324016849989383083842550171",
    //    "refundId":"50000408952018110906966070323",
    //    "refundCount":1,
    //    "refundStatus":"QUERY_REFUND_SUCCESS",
    //    "refundSuccessTime":"2018-11-09 14:44:07"
    //}
```
```
    /**
     * 打款
     * <p>
     *     如果是微信(公众平台)，则主要实现付款到零钱
     * </p>
     * @param partnerTradeNo    商户自定义的打款单号
     * @param userName          用户真实姓名
     * @param amount            打款金额，精度：分
     * @param desc              打款描述
     * @param openid            微信打款到零钱时使用,支付宝平台是对应用户的收款账号
     * @return                  打款信息
     */
    String transfers(String partnerTradeNo,String userName, int amount, String desc, String openid);

    //示例返回值：
    //{
    //    "partnerTradeNo":"12261244335370718184846044428624",
    //    "paymentNo":"1454246502201811098705748974",
    //    "paymentTime":"2018-11-09 14:47:31"
    //}
```
```
    /**
     * 查询企业付款状态
     * <p>
     *     如果是微信(公众平台)，则主要实现付款到零钱查询
     * </p>
     * @param partnerTradeNo    商户自定义的打款单号
     * @return                  打款结果
     */
    String getTransferInfo(String partnerTradeNo);

    //示例返回值：
    //{
    //    "partnerTradeNo":"12261244335370718184846044428624",
    //    "paymentNo":"1454246502201811098705748974",
    //    "status":"QUERY_TRANSFER_SUCCESS",
    //    "reason":"",
    //    "openid":"oIaRgxPwen6D5EUrreArhB3tO2QA",
    //    "userName":"xxx",
    //    "fee":"1.00",
    //    "transferTime":"2018-11-09 14:47:31",
    //    "paymentTime":"2018-11-09 14:47:31",
    //    "desc":"红包"
    //}
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request