package com.sinsz.pay.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 查询付款信息
 * @author chenjianbo
 */
public class TransferQueryDto {

    /**
     * 商家自定义打款单号
     */
    private String partnerTradeNo;

    /**
     * 支付平台付款单号
     */
    private String paymentNo;

    /**
     * 转账状态
     */
    private String status;

    /**
     * 失败原因
     */
    private String reason;

    /**
     * 微信的公众号唯一ID
     */
    private String openid;

    /**
     * 收款人用户真实姓名
     */
    private String userName;

    /**
     * 收款金额
     * 显示为：0.01元 格式
     */
    private String fee;

    /**
     * 转账时间
     */
    private String transferTime;

    /**
     * 转账成功时间
     */
    private String paymentTime;

    /**
     * 付款备注
     */
    private String desc;

    public String getPartnerTradeNo() {
        return partnerTradeNo;
    }

    public void setPartnerTradeNo(String partnerTradeNo) {
        this.partnerTradeNo = partnerTradeNo;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(String transferTime) {
        this.transferTime = transferTime;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
