package com.sinsz.pay.util;

import com.sinsz.common.exception.ApiException;
import com.sinsz.pay.support.Constant;
import org.apache.commons.codec.Charsets;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author chenjianbo
 */
public final class PayUtils {

    /**
     * 生成默认6位一次性密码
     *
     * @return 一次性密码
     */
    public synchronized static String otp() {
        return OTPUtils.generateTOTP(
                UUID.randomUUID().toString().replaceAll("-", ""),
                System.currentTimeMillis() + "",
                6
        );
    }

    /**
     * 生成一次性密码
     *
     * @param len 一次性密码长度
     * @return 一次性密码
     */
    public synchronized static String otp(int len) {
        len = Math.abs(len);
        len = len > 8 ? 8 : len;
        return len == 6 ? otp() : OTPUtils.generateTOTP(
                UUID.randomUUID().toString().replaceAll("-", ""),
                System.currentTimeMillis() + "",
                len
        );
    }

    /**
     * 字符串MD5转换
     *
     * @param arg 输入字符串
     * @return MD5字符串
     */
    public synchronized static String md5(String arg) {
        try {
            char[] hexDigits = {
                    '0', '1', '2', '3', '4', '5',
                    '6', '7', '8', '9', 'a', 'b',
                    'c', 'd', 'e', 'f'
            };
            byte[] strTemp = arg.getBytes(StandardCharsets.UTF_8);
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(strTemp);
            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace(System.out);
        }
        return "";
    }

    /**
     * 计算文件MD5
     *
     * @param in 文件输入流
     * @return MD5码
     */
    public synchronized static String md5(FileInputStream in) {
        try (FileInputStream arg = in) {
            FileChannel fc = arg.getChannel();
            MappedByteBuffer byteBuffer = fc.map(
                    FileChannel.MapMode.READ_ONLY,
                    0,
                    fc.size()
            );
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(byteBuffer);
            BigInteger bi = new BigInteger(1, md5.digest());
            return bi.toString(16);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return "";
    }

    /**
     * 集合中泛型对象类型转换
     *
     * @param list 源集合参数
     * @param func 函数参数
     * @param <T>  源集合元素类型
     * @param <R>  目标集合元素类型
     * @return 目标集合对象
     */
    public synchronized static <T, R> List<R> conversionList(List<T> list, Function<T, R> func) {
        List<R> results = new ArrayList<>();
        if (list == null || list.isEmpty()) {
            return results;
        }
        for (T item : list) {
            results.add(func.apply(item));
        }
        return results;
    }

    /**
     * 字符串生成转换为hash路径
     * <p>
     * 可用于文件名称生成保存文件的路径
     * </p>
     *
     * @param str 源字符串
     * @return 字符串路径
     */
    public synchronized static String stringToHashUrl(String str) {
        int hash = str.hashCode();
        if (hash < 0) {
            hash = Math.abs(hash);
        }
        int len = 2;
        StringBuilder builder = new StringBuilder();
        builder.append(File.separator);
        for (int i = 0; i < len; i++) {
            if (hash <= 256) {
                if (i == 0) {
                    builder.append(frontCompWithZore(0, 3))
                            .append(File.separator);
                }
                builder.append(frontCompWithZore(hash, 3))
                        .append(File.separator);
                break;
            } else {
                builder.append(frontCompWithZore(hash % 256, 3))
                        .append(File.separator);
                hash = hash / 1000;
            }
        }
        return builder.append(str).toString();
    }

    /**
     * 时间格式化
     * <p>
     * 字符串精确到毫秒
     * </p>
     *
     * @param date 时间
     * @return 字符串
     */
    public synchronized static String timeToStringMill(Date date) {
        return String.format("%tF %tT.%tL", date, date, date);
    }

    /**
     * 时间格式化
     * <p>
     * 字符串精确到天
     * </p>
     *
     * @param date 时间
     * @return 字符串
     */
    public synchronized static String timeToStringDay(Date date) {
        return String.format("%tF", date);
    }

    /**
     * 时间格式化
     * <p>
     * 字符串精确到秒
     * </p>
     *
     * @param date 时间
     * @return 字符串
     */
    public synchronized static String timeToString(Date date) {
        return String.format("%tF %tT", date, date);
    }

    /**
     * 元数据添0补位
     *
     * @param arg 源数据
     * @param len 位数
     * @return 结果
     */
    public synchronized static String frontCompWithZore(int arg, int len) {
        return String.format("%0" + len + "d", arg);
    }

    /**
     * 驼峰转换：下划线转驼峰
     *
     * @param arg0 源数据
     * @return 驼峰数据
     */
    public synchronized static String lineToHump(String arg0) {
        if (arg0 == null || "".equals(arg0.trim())) {
            return "";
        }
        int len = arg0.length();
        StringBuilder builder = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = arg0.charAt(i);
            if (c == '_') {
                if (++i < len) {
                    builder.append(Character.toUpperCase(arg0.charAt(i)));
                }
            } else {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    /**
     * 驼峰转换：驼峰转下划线字符串
     *
     * @param arg0 元数据
     * @return 下划线字符串
     */
    public synchronized static String humpToLine(String arg0) {
        if (arg0 == null || "".equals(arg0.trim())) {
            return "";
        }
        int len = arg0.length();
        StringBuilder builder = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = arg0.charAt(i);
            if (Character.isUpperCase(c)) {
                builder.append("_");
                builder.append(Character.toLowerCase(c));
            } else {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    /**
     * BASE64 编码
     *
     * @param str 参数
     * @return 编码后的字符串
     */
    public synchronized static String baseEncode(String str) {
        return Base64.getEncoder().encodeToString(
                str.getBytes(StandardCharsets.UTF_8)
        );
    }

    /**
     * BASE64 解码
     *
     * @param str 参数
     * @return 解码后的参数
     */
    public synchronized static String baseDecode(String str) {
        return new String(
                Base64.getDecoder().decode(str),
                StandardCharsets.UTF_8
        );
    }

    /**
     * 将字符串倒序
     *
     * @param str 源字符串
     * @return 倒序后的字符串
     */
    public synchronized static String reverse(String str) {
        char[] chars = str.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (int i = chars.length - 1; i >= 0; i--) {
            builder.append(chars[i]);
        }
        return builder.toString();
    }

    /**
     * 判断字符串是否为空
     *
     * @param str 原始字符串
     * @return true表示是空的；false表示非空
     */
    public synchronized static boolean isNull(String str) {
        return str == null || "".equals(str.trim());
    }

    /**
     * 判断字符串是否为空
     *
     * @param str 原始字符串
     * @return true表示非空；false表示是空的；
     * @link isNull(...)
     */
    public synchronized static boolean isNotNull(String str) {
        return !isNull(str);
    }


    private static final String IP_MATCHES = "(?=(\\b|\\D))(((\\d{1,2})|(1\\d{1,2})|(2[0-4]\\d)|(25[0-5]))\\.){3}(" +
            "(\\d{1,2})|(1\\d{1,2})|(2[0-4]\\d)|(25[0-5]))(?=(\\b|\\D))";

    /**
     * 网络地址IP验证
     *
     * @param ip 地址
     * @return 是否为IP地址
     */
    public synchronized static boolean isNetIP(String ip) {
        return PayUtils.isNotNull(ip) && Pattern.matches(IP_MATCHES, ip);
    }

    /**
     * 将字节数组前4个字节转换为整形数
     * @param bytes 字节数组
     * @return  返回整形数字结果
     */
    public synchronized static int bytesToInt(byte[] bytes) {
        byte[] b = new byte[4];
        if (bytes != null) {
            int len = bytes.length < 4 ? bytes.length: 4;
            System.arraycopy(bytes, 0, b, 0, len);
        }
        return (((b[0] & 0xff) << 24) |
                ((b[1] & 0xff) << 16) |
                ((b[2] & 0xff) <<  8) |
                ((b[3] & 0xff)));
    }

    /**
     * 将整形数转换为4个字节的字节数组
     * @param arg   目标整形数
     * @return 返回字节数组
     */
    public synchronized static byte[] intToBytes(int arg) {
        byte[] result = new byte[4];
        result[0] = (byte)((arg >> 24) & 0xFF);
        result[1] = (byte)((arg >> 16) & 0xFF);
        result[2] = (byte)((arg >> 8) & 0xFF);
        result[3] = (byte)(arg & 0xFF);
        return result;
    }


    /**
     * 格式化完整回调地址
     * @param api
     * @return
     */
    public static String redirect(String def, String api, boolean encode) {
        List<String> list = Stream.of("http://", "https://")
                .filter(def::contains)
                .collect(Collectors.toList());
        if (list.isEmpty() || list.size() != 1) {
            throw new ApiException(Constant.DEFAULT_EXCEPTION_CODE, "微信授权网址不合法.");
        }
        String http = def.lastIndexOf("/") < def.length() - 1 ? def : def.substring(0, def.length() -1);
        String path = http + api;
        try {
            if (encode) {
                return URLEncoder.encode(path, Charsets.UTF_8.name());
            } else {
                return path;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(System.out);
            throw new ApiException(Constant.DEFAULT_EXCEPTION_CODE, "微信授权获取地址编码异常.");
        }
    }

}
